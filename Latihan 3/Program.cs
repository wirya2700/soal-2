﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Latihan_3
{
    class Program
    {
        static void TulisSpasi(Int32 jumlahSpasi)
        {
            for (int j = 1; j <= jumlahSpasi; j++)
            {
                Console.Write(" ");
            }
        }

        static void HitungBintang(Int32 jumlahBintang)
        {
            Int32 totalBintang = 0;
            Int32 jumlahSpasi = 0;

            for (int i = 1; i <= jumlahBintang; i++)
            {
                jumlahSpasi = jumlahBintang - i;
                TulisSpasi(jumlahSpasi);

                for (int j = 1; j <= i * 2 -1; j++)
                {
                    Console.Write("*");
                    totalBintang += 1;
                }

                TulisSpasi(jumlahSpasi);
                Console.WriteLine();
            }

            Console.WriteLine("Total bintang kamu : " + totalBintang);
            Pertanyaan();
            Console.ReadKey();
        }
        


        static void Pertanyaan()
        {
            Int32 jumlahBintang;
            Console.WriteLine("Masukan jumlah bintang :");
            jumlahBintang = Convert.ToInt32(Console.ReadLine());
            HitungBintang(jumlahBintang);

        }

        static void Main(string[] args)
        {
            Pertanyaan();
        }
    }
}
